import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product/product.service';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  providers: [ProductService]
})
export class ProductListComponent implements OnInit {
  public products;
  public erro = false;
  constructor(private productService: ProductService) {
    this.getProducts();
   }

  getProducts() {
    return this.productService.getProducts().subscribe(
                     products => this.products = this.verifyProduct(products),
                     error =>  this.erro = true);
  }

  ngOnInit() {
  }

  verifyProduct(products) {
    let prods;
    prods = products.map(function(p){
      if(p.image == null) p.image = 'assets/img/no-image.png';
      return p;
    });
    this.setProductToLocal(prods);
    return prods;
  }

  addToCart(product){
    let showOrHide = this.productService.insertToCart(product);

    if(showOrHide){
      const el = document.getElementById("alert__product--added");
      this.fadeElement(el);
    } else {
      const el = document.getElementById("alert__product--removed");
      this.fadeElement(el);
    }
  }

  fadeElement(el) {
      el.style.visibility = 'visible';
      el.style.opacity = '1';
      setTimeout(function(){ 
        el.style.visibility = 'hidden';
        el.style.opacity = '0';
      }, 1500);
  }

  setProductToLocal (prods) {
    if(prods != null){
        localStorage.setItem("products", JSON.stringify(prods));   
    } else{
        localStorage.setItem("products", JSON.stringify([])); 
    }
  }
}
