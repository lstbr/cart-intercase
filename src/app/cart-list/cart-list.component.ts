import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product/product.service';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.css'],
  providers: [ProductService]
})
export class CartListComponent implements OnInit {

  public cart = [];
  public totalQuantity = 0;
  constructor(private productService: ProductService) {
    this.cart = JSON.parse(localStorage.getItem("cart"));
    this.calculateTotal();
   }

  ngOnInit() {
  }

  addToCart(product){
    this.cart = this.productService.insertQuantity(product);
    this.calculateTotal();
  }

  removeFromCart(product) {
    this.cart = this.productService.removeFromCart(product);
    this.calculateTotal();
  }

  calculateTotal() {
    return this.totalQuantity = this.cart.reduce((result, current) => result+(current.quantity*current.price), 0);
  }

}
