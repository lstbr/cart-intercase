import { Injectable } from '@angular/core';
import { Product } from './product';
import { environment } from '../../../environments/environment';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class ProductService {

    public cart = [];
    public products = [];
    constructor (private http: Http) {
        if(localStorage.getItem("cart") == null){
            localStorage.setItem("cart", JSON.stringify([]));   
        }
    }

    getProducts () {
        if(localStorage.getItem("cart") == null){
            localStorage.setItem("cart", JSON.stringify([]));   
        }
        return this.http.get(environment.host)
                        .map(response => response.json() || {});
    }

    insertToCart(product) {
        let res= false;
        let newProduct = {
            "image": product.image,
            "name": product.name,
            "price": product.price,
            "quantity": 1
        };
        this.cart = JSON.parse(localStorage.getItem("cart"));
        let existentProduct = this.cart.filter(function(product){
            return product.name === newProduct.name;
        });

        if (existentProduct.length) {
            if(existentProduct[0].quantity < product.quantity){
                existentProduct[0].quantity += 1;
                res= true;
            }else{
                res= false;
            }
            localStorage.setItem('cart', JSON.stringify(this.cart));
        } else {
            newProduct.quantity = 1;
            this.cart.unshift(newProduct);
            localStorage.setItem('cart', JSON.stringify(this.cart));
            res= true;
        }

        return res;
    }

    insertQuantity(product){
        this.cart = JSON.parse(localStorage.getItem("cart"));
        let existentProductCart = this.cart.filter(function(newProduct){
            return product.name === newProduct.name;
        });
        this.products = JSON.parse(localStorage.getItem("products"));
        let existentProduct = this.products.filter(function(newProduct){
            return product.name === newProduct.name;
        });
        if(existentProduct[0].quantity > existentProductCart[0].quantity){
            existentProductCart[0].quantity += 1;
            localStorage.setItem('cart', JSON.stringify(this.cart));
            return this.cart;
        } else{
            return this.cart;
        }
    }
    removeFromCart(product) {
        let self = this;
        this.cart = JSON.parse(localStorage.getItem("cart"));

        this.cart.forEach(function(item, index) {
            if (product.name === item.name) {
                if (item.quantity > 1) {
                item.quantity -= 1;
                } else {
                self.cart.splice(index, 1);
                }
            }
        });
        localStorage.setItem('cart', JSON.stringify(this.cart));
        return this.cart;
    }
}
