# CartInter

Neste projeto utilizei o Angular CLI para gerar o projeto, ele já vem com comandos prontos para rodar e buildar e projeto. [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-beta.32.3.

## Iniciar Servidor localhost

Primeiro é necessário rodar o comando `npm install` para baixar todas as dependências do projeto.

Após isso basta rodar o comando `npm start` pois vai iniciar o server no seguinte endereço `http://localhost:4200/`. Esse comando roda concorrentemente o `ng serve` junto com `gulp sass:watch`. Sendo assim você poderá desenvolver com SCSS e o seu código será compilado.

Caso tenha algum problema ao iniciar o servidor instale o Angular CLI globalmente com o seguinte comando: 
`npm install -g @angular/cli`.

## Tecnologias utilizadas

Neste projeto decidi utilizar o Angular 2 pelo fato da componetização e separação de responsabilidade de cada componente, com ele eu posso criar um arquivo html, css e js de cada componente assim pude explorar outras ferramentas como o SCSS e assim utilizar o GULP para compilar pra CSS. Utilizei também o POSTCSS com o plugin de autoprefixer pois assim não precisaria me preocupar em lidar com cross-browser, por este fato utilizei o FLEXBOX para controlar o posicionamento do conteúdo. E para organização do código CSS, decidi usar BEM pela sua filosofia de componentes, cada componente crido é um bloco do BEM contendo seus elementos. As outras metodologias que existe para criação focam mais em grandes projetos. Para meu reset utilizei o NORMALIZE para oferecer um visual default a todos os elementos em qualquer navegador, principalmente nas fontes e espaçamentos.  


## Mais informações sobre o CLI

Para mais informações sobre Angular CLI use `ng help` ou vá na documentação [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
