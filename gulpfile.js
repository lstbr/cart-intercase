'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');

gulp.task('default', ['sass:watch'])

gulp.task('sass', function () {
  return gulp.src('./src/app/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(postcss([autoprefixer({
      browsers: ['last 9 versions']
    })]),
    cssnano())
    .pipe(gulp.dest('./src/app'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./src/app/**/*.scss', ['sass']);
});