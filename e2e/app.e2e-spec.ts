import { CartInterPage } from './app.po';

describe('cart-inter App', () => {
  let page: CartInterPage;

  beforeEach(() => {
    page = new CartInterPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
